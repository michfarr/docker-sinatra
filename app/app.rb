require 'sinatra'
require 'sinatra/json'
require 'sinatra/activerecord'

set :database_file, 'config/database.yml'

class Resource < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
end

# index
get '/' do
  json Resource.select('id', 'name').all
end

# show
get '/:id' do
  resource =  Resource.find_by_id(params[:id])

  if resource
    halt 206, json(resource)
  else
    halt 404
  end
end

# create
post '/' do
  resource = Resource.create(params)

  if resource
    json resource
  else
    halt 500
  end
end

# update
patch '/:id' do
  resource = Resource.find_by_id(params[:id])

  if resource
    resource.update(params)
  else
    halt 404
  end
end

# delete
delete '/:id' do
  resource = Resource.find_by_id(params[:id])

  if resource
    resource.destroy
  else
    halt 404
  end
end
